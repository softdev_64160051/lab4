/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1, player2, currentPlyer;
    private int row, col;
    private int count;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlyer = player1;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlyer() {
        return currentPlyer;
    }

    public boolean setRowCol(int row, int col) {
        if (table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currentPlyer.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        if (checkRow()) {
            saveWin();
            return true;
        }
        if (checkCol()) {
            saveWin();
            return true;
        }
        if (checkX1()) {
            saveWin();
            return true;
        }
        if (checkX2()) {
            saveWin();
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        if (count == 9) {
            player1.draw();
            player2.draw();
            return true;
        }
        return false;
    }

    private boolean checkRow() {
        return table[row - 1][0] != '-' && table[row - 1][0] == table[row - 1][1]
                && table[row - 1][0] == table[row - 1][2];
    }

    private boolean checkCol() {
        return table[0][col - 1] != '-' && table[0][col - 1] == table[1][col - 1]
                && table[0][col - 1] == table[2][col - 1];
    }

    private boolean checkX1() {
        return table[0][0] == currentPlyer.getSymbol() && table[1][1] == currentPlyer.getSymbol()
                && table[2][2] == currentPlyer.getSymbol();
    }

    private boolean checkX2() {
        return table[0][2] == currentPlyer.getSymbol() && table[1][1] == currentPlyer.getSymbol()
                && table[2][0] == currentPlyer.getSymbol();
    }

    private void saveWin() {
        if (currentPlyer == player1) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    void switchPlayer() {
        if (currentPlyer == player1) {
            currentPlyer = player2;
        } else {
            currentPlyer = player1;
        }
    }

}
